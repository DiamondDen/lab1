package net.lav.lab1;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    System.out.println("What's your name??!");
    String name = new Scanner(System.in).nextLine();
    System.out.printf("Hi %s!)", name);
  }
}
